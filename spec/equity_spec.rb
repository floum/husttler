# frozen_string_literal: true

RSpec.describe EquityCalculator do
  context "on AsAdThTc2d" do
    let(:board) do
      Board.new("AsAdThTc2d")
    end
    context "against KdQd, KdTd" do
      let(:villain_range) do
        HRange.new(
          %w[
            KdQd
            KdTd
          ]
        )
      end

      it "9d8d has 0 equity" do
        equity_calculator = EquityCalculator.new(board: board, villain_range: villain_range)
        holding = Holding.new("9d8d")
        expect(equity_calculator.compute(holding)).to eq 0
      end

      it "AcQh has 1 equity" do
        equity_calculator = EquityCalculator.new(board: board, villain_range: villain_range)
        holding = Holding.new("AcQh")
        expect(equity_calculator.compute(holding)).to eq 1
      end
    end
  end
end
