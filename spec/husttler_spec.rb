# frozen_string_literal: true

RSpec.describe Husttler do
  it "has a version number" do
    expect(Husttler::VERSION).not_to be nil
  end
end
