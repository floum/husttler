# frozen_string_literal: true

require "hand_rank"

require_relative "husttler/version"
require_relative "husttler/equity_calculator"
require_relative "husttler/board"
require_relative "husttler/range"
require_relative "husttler/holding"
require_relative "husttler/card"

module Husttler
  class Error < StandardError; end
  # Your code goes here...
end
