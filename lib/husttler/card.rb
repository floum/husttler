# frozen_string_literal: true

module Husstler
  class Card
    def initialize(repr)
      @suit = repr[1]
      @rank = repr[0]
    end

    def hand_rank_value
      rank_value + suit_value
    end

    def rank_value
      %w[2 3 4 5 6 7 8 9 T J Q K A].index(@rank)
    end

    def suit_value
      %w[c d h s].index(@suit) + 1
    end
  end
end
