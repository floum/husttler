# frozen_string_literal: true

module Husstler
  class HRange
    attr_reader :holdings

    def initialize(holdings)
      @holdings = holdings.map { |holding| Holding.new(holding) }
    end
  end
end
