# frozen_string_literal: true

module Husstler
  class EquityCalculator
    def initialize(attrs = {})
      @board = attrs[:board]
      @villain_range = attrs[:villain_range]
    end

    def compute(holding)
      holding_ranking = @board.rank(holding)
      villain_rankings = @villain_range.holdings.map do |villain_holding|
        @board.rank(villain_holding)
      end

      (villain_rankings.map do |villain_ranking|
        holding_ranking <=> villain_ranking
      end.reduce(:+) + villain_rankings.size) / (2 * villain_rankings.size)
    end
  end
end
