# frozen_string_literal: true

module Husstler
  class Holding
    attr_reader :cards

    def initialize(cards)
      @cards = cards.chars.each_slice(2).map { |card| Card.new(card) }
    end
  end
end
