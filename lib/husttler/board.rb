# frozen_string_literal: true

module Husstler
  class Board
    def initialize(cards)
      @cards = cards.chars.each_slice(2).map { |card| Card.new(card) }
    end

    def rank(holding)
      HandRank.get(@cards.map(&:hand_rank_value) + holding.cards.map(&:hand_rank_value))
    end
  end
end
